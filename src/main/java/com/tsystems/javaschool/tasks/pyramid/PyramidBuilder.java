package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.contains(null)
                || !PyramidCanvas.canBuildPyramid(inputNumbers.size())) {
            throw new CannotBuildPyramidException();
        }

        List<Integer> sortedInputNumbers = sortInputNumbers(inputNumbers);//done
        int quantityOfInputNumbers = sortedInputNumbers.size(); //done
        int canvasRows = PyramidCanvas.calculateCanvasRows(quantityOfInputNumbers);
        int canvasColumns = PyramidCanvas.calculateCanvasColumns(canvasRows);
        int canvasMiddle = PyramidCanvas.calculateCanvasMiddle(canvasColumns);

        int[][] canvas = new int [canvasRows][canvasColumns];
        PyramidCanvas.setVacantPositionsWithNumber(0, canvas);
        PyramidCanvas.fillCanvas(canvas, canvasMiddle, sortedInputNumbers);
        return canvas;
    }

    private List<Integer> sortInputNumbers(List<Integer> inputNumbers) {
        List<Integer> numbers = new ArrayList<>(inputNumbers);
        Collections.sort(numbers);
        return numbers;
    }

    private static class PyramidCanvas {

        private static boolean canBuildPyramid(int quantityOfNumbers) {

            double rows = calculateCanvasRows(quantityOfNumbers);

            if((quantityOfNumbers > ((Integer.MAX_VALUE/2) + 1) ||
                    quantityOfNumbers < 3 || quantityOfNumbers != ((double)rows * (1.0 + (double)rows)) / 2.0)) {
                return false;
            }
            return true;
        }

        private static int calculateCanvasRows(int quantityOfInputNumbers) {
            return (int) quadraticEqu(1,- 1,(2 - (quantityOfInputNumbers * 2)));
        }

        private  static double quadraticEqu(double a, double b, double c)
        {
            double d = b*b - 4*a*c;
            if (d > 0)
            {
                double x = (-b + Math.sqrt(d))/(2*a);
                return Math.abs(x);
            }
            if (Math.abs(d) < 10E-6)
            {
                return -b/2*a;
            }
            return 0;
        }
        private static int calculateCanvasColumns(int quantityOfCanvasRows) {
            return quantityOfCanvasRows + ( quantityOfCanvasRows - 1);
        }

        private  static int calculateCanvasMiddle(int quantityOfCanvasColumns) {
            return quantityOfCanvasColumns / 2;
        }

        private static void setVacantPositionsWithNumber(int number, int[][] canvas) {
            for(int i = 0; i < canvas.length; i++) {
                for(int j = 0; j < canvas[i].length; j++) {
                    canvas[i][j]  = 0;

                }
            }
        }

        private static void fillCanvas(int [][] canvas, int middleOfCanvas, List<Integer> inputNumbers) {
            int mid = middleOfCanvas;
            int inputNumberIndex = 0;
            for(int i = 0; i < canvas.length; i++) {
                int startIndex = mid;
                for(int j = 0; j < i + 1; j++) {
                    canvas[i][startIndex] = inputNumbers.get(inputNumberIndex++);
                    startIndex += 2;
                }
                mid--;
            }
        }
    }
}
