package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.function.Function;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public static final String STATEMENT_PATTERN = "(?!.*\\((\\d+|\\d+\\.\\d+)\\).*)\\(?(?:\\d+|\\d+\\.\\d+)(:?[-+*/]\\(?(?:\\d+|\\d+\\.\\d+)\\)?)+\\)?";
    public static final Pattern PARENTHESES_PATTERN = Pattern.compile("\\([^)(]+\\)");
    private boolean divisionByZero = false;

    public String evaluate(String statement) {
        String result = statement;

        if(!isValid(statement)) {
            return null;
        }

        if(containsParentheses(result)) {
            result = evaluateInParentheses(statement,PARENTHESES_PATTERN);
        }

        result = simpleEvaluation(result);

        return (result == null? result : formatResult(result));
    }

    private boolean isValid(String statement) {
        if(statement == null||statement.length() < 3) {
            return false;
        }

        if(statement.matches(STATEMENT_PATTERN)) {
            if(containsParentheses(statement)) {
                if(isParenthesesBalanced(statement)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return true;
        }
    return false;
    }

    private boolean containsParentheses(String statement) {
        return statement.contains("(")||statement.contains(")");
    }

    private boolean isParenthesesBalanced(String statement) {
        Deque<String> deque = new LinkedList<>();

        for (String ch: statement.split("")) {
            if (ch.equals("(")) {
                deque.addFirst(ch);
            }
            else if (ch.equals(")")) {
                if (!deque.isEmpty() && (deque.peekFirst().equals("("))) {
                    deque.removeFirst();
                }
                else {
                    System.out.println("imbalanced");
                    return false;
                }
            }
        }

        if (!deque.isEmpty()) {
            return false;
        }

        return true;
    }

    private String evaluateInParentheses(String statement, Pattern pattern) {
        String result = statement;
        Matcher matcher = pattern.matcher(result);

        while (matcher.find()) {
            String statementFromParentheses = matcher.group();
            result = matcher.replaceFirst(simpleEvaluation(statementFromParentheses.replaceAll("[)(]", "")));
            matcher.reset(result);
        }

        return result;
    }

    private String simpleEvaluation(String statement){
        String result = statement;

        if(result.contains("/")||result.contains("*")) {
            Pattern pattern = Pattern.compile("(?:\\d+\\.\\d+|\\d+)[*/]\\-?(?:\\d+\\.\\d+|\\d+)");
            Matcher matcher = pattern.matcher(statement);
            while (matcher.find() && !divisionByZero) {
                result = matcher.replaceFirst(divideMultiply(matcher.group()));
                matcher.reset(result);
            }
        }

        if(divisionByZero) {
            return null;
        }

        if(result.contains("+") || result.contains("-")) {
            result = plusMinus(result);
        }

        return result;
    }
    private String divideMultiply(String statement) {
        String result = statement;
        if(result.contains("*"))
        {
            return multiply(result);
        }
        else {
            return divide(result);
        }
    }


    private String plusMinus(String statement) {
        if(statement.matches("\\-(?:\\d+\\.\\d+|\\d+)")) {
            return statement;
        }

        String result = statement;

        Deque<String> stack  = new ArrayDeque<>();
        String[] tokens = result.split("\\b(?=[+-]|(?<=[+-])\\b)");

        for(String token: tokens) {
            stack.add(token);
        }

        while(stack.size() > 1) {
            double left  = Double.parseDouble(stack.pop());
            String operator = stack.pop();
            double right = Double.parseDouble(stack.pop());
            double evaluation = 0;
            switch (operator) {
                case "+": evaluation = left + right;
                    break;

                case "-": evaluation = left - right;
                    break;
            }
            stack.push(String.valueOf(evaluation));
        }

        result =  stack.pop();

        return result;
    }

    private String multiply(String statement) {
        String[] operands = statement.split("\\*");
        double left  = Double.parseDouble(operands[0]);
        double right = Double.parseDouble(operands[1]);
        double result = left * right;

        return String.valueOf(result);
    }

    private String divide(String statement) {
        String[] operands = statement.split("/");
        double left  = Double.parseDouble(operands[0]);
        double right = Double.parseDouble(operands[1]);

        if(right == 0) {
           divisionByZero = true;
        }

        double result = left/right;

        return String.valueOf(result);
    }

    private  String formatResult(String result) {
        String formattedResult;
        Pattern decimalPattern = Pattern.compile("[-]?(\\d+\\.0+|\\d+)");
        Matcher matcher = decimalPattern.matcher(result);

        if(matcher.matches()) {
            matcher.reset();
            matcher.usePattern(Pattern.compile("[-]?\\d+"));
            matcher.find();
            formattedResult = matcher.group();
        }
        else {
            BigDecimal bd = new BigDecimal(result);
            bd = bd.setScale(4, RoundingMode.HALF_UP);
            formattedResult =  String.valueOf(bd.doubleValue());
        }
        return formattedResult;
    }
}
